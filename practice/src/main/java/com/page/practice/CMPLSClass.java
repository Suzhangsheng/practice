package com.page.practice;

import java.util.Scanner;

/**
 *  CMPLS - Complete the Sequence!
 *
 * Created by Page on 2015/12/21.
 */
public class CMPLSClass {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int s;
        int c;
        int[][] f = new int[102][102];
        for (int i = 0; i < n; i++) {
            s = sc.nextInt();
            c = sc.nextInt();

            for (int j = 0; j < s; j++) {
                f[0][j] = sc.nextInt();
            }

            for (int j = 1; j < s; j++) {
                for (int k = 0; j + k < s; k++) {
                    f[j][k] = f[j-1][k+1] - f[j-1][k];
                }
            }

            for (int j = 1; j <= c; j++) {
                f[s-1][j] = f[s-1][0];
            }

            for (int j = s - 2; j >= 0; j--) {
                for (int k = s - j; k < s + c; k++) {
                    f[j][k] = f[j+1][k-1] + f[j][k-1];
                }
            }

            for (int j = s; j < s + c; j++) {
                System.out.print(f[0][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}
