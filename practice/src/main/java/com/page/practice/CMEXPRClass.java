package com.page.practice;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * CMEXPR - Complicated Expressions
 * Created by Page on 2015/12/21.
 */
public class CMEXPRClass {

    public static Map<String, Integer> map = new HashMap<String, Integer>();
    static {
        map.put("+", 0);
        map.put("-", 0);
        map.put("*", 1);
        map.put("/", 1);
    }
    private static char[] expArray;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            String expression = sc.next();
            simplify(expression);
        }
    }

    private static void simplify(String expression) {
        expArray = expression.toCharArray();
        Node root = makeTree(0, expArray.length - 1);
        System.out.println(root.condense());
    }

    public static Node makeTree(int l, int r) {
        while(true) {
            if(l == r) {
                Node node = new Node();
                node.setOp(expArray[l]);
                node.setLeft(null);
                node.setRight(null);
                return node;
            }

            int pre, br, i;

            for (pre = 0; pre <= 1; pre++) {
                br = 0;
                for (i = r; i >= l; i--) {
                    if(expArray[i] == '(') {
                        br--;
                    } else if(expArray[i] == ')') {
                        br++;
                    } else if(br == 0 && precedence(expArray[i]) == pre) {
                        Node node = new Node();
                        node.setLeft(makeTree(l, i-1));
                        node.setRight(makeTree(i+1, r));
                        node.setOp(expArray[i]);
                        return node;
                    }
                }
            }

            l++;
            r--;
        }
    }

    private static int precedence(char op) {
        if(Character.isAlphabetic(op)) {
            return 2;
        } else {
            return map.get(String.valueOf(op));
        }
    }

    static class Node {
        private Node left;
        private Node right;
        private char op;

        public String condense() {
            if(Character.isAlphabetic(op)) {
                return String.valueOf(op);
            }
            String lexp = "";
            String rexp = "";

            int lpre = 0;
            int rpre = 0;

            int pre = precedence(op);
            if (left != null) {
                lexp = left.condense();
                lpre = precedence(left.getOp());
            }
            if (right != null) {
                rexp = right.condense();
                rpre = precedence(right.getOp());
            }

            if (pre > rpre) {
                rexp = "(" + rexp + ")";
            }
            if (pre > lpre) {
                lexp = "(" + lexp + ")";
            }
            if (pre == rpre && (op == '-' || op == '/')) {
                rexp = "(" + rexp + ")";
            }

            return lexp + op + rexp;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public void setRight(Node right) {
            this.right = right;
        }

        public char getOp() {
            return op;
        }

        public void setOp(char op) {
            this.op = op;
        }
    }
}


