package com.page.practice;

import java.util.Scanner;

/**
 * IKEYB - I-Keyboard
 * Created by Page on 2015/12/24.
 */
public class IKEYBClass {

    private static final int MAX = 91;

    private static int[][] cost = new int[MAX][MAX];
    private static int[][] price = new int[MAX][MAX];
    private static int[][] index = new int[MAX][MAX];

    private static void initialize(int L, int F[]) {
        for (int i = 0; i < MAX; i++) {
            for (int j = 0; j < MAX; j++) {
                price[i][j] = Integer.MAX_VALUE / 2;
            }
        }
        price[0][0] = 0;
        for (int i = 1; i <= L; i++) {
            for (int j = i; j <= L; j++) {
                cost[i][j] = cost[i][j-1] + (j-i+1) * F[j-1];
            }
        }
    }

    private static void compute(int K, int L) {
        for (int i = 1; i <= K; i++) {
            for (int j = i; j <= L; j++) {
                for (int n = 1; n <= j-i+1; n++) {
                    int sum = price[i-1][j-n] + cost[j-n+1][j];
                    if (sum < price[i][j]) {
                        price[i][j] = sum;
                        index[i][j] = n;
                    }
                }
            }
        }
    }

    private static void output(int K, int L, char keys[], char letters[]) {
        if (K == 0) {
            return;
        }
        output(K-1, L-index[K][L], keys, letters);
        System.out.print(keys[K-1]+": ");
        for (int i = L - index[K][L]; i < L; i++) {
            System.out.print(letters[i]);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] F = new int[MAX-1];
        int T;
        Scanner scanner = new Scanner(System.in);
        T = scanner.nextInt();
        for (int K, L, n = 1; n <= T; n++) {
            K = scanner.nextInt();
            L = scanner.nextInt();
            char[] keys = scanner.next().toCharArray();
            char[] letters = scanner.next().toCharArray();
            for (int i = 0; i < L; i++) {
                F[i] = scanner.nextInt();
            }
            initialize(L, F);
            compute(K, L);
            System.out.println("Keypad #"+n);
            output(K, L, keys, letters);
            System.out.println();
        }
    }
}
