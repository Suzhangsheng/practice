package com.page.practice;

import java.util.Scanner;

/**
 * FCTRL - Factorial
 * Created by Page on 2015/12/24.
 */
public class FCTRLClass {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            int num = sc.nextInt();
            System.out.println(countZero(num));
        }
        System.out.println("cost " + (System.currentTimeMillis() - start) + "ms");
    }

    private static int countZero(int n) {
        int power = 0;
        for (; Math.pow(5, power) <= n; power++);
        power--;

        int count = 0;
        for (int i = 5; i <= n; i=i+5) {
            for (int j = power; j > 0; j--) {
                if (i % Math.pow(5, j) == 0) {
                    count += j;
                    break;
                }
            }
        }

        return count;
    }

    private static int countZero2(int n) {
        int result = 0;
        int five = 5;
        while(n>five)
        {
            result+=n/five;
            five*=5;
        }
        return result;
    }
}
